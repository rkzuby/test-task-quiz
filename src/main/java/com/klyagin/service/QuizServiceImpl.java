package com.klyagin.service;

import com.klyagin.dao.QuizDao;
import com.klyagin.model.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizServiceImpl implements QuizService {
    @Autowired
    private QuizDao quizDao;

    @Override
    public List<Quiz> findAllQuizes() {
        return quizDao.findAll();
    }
}
