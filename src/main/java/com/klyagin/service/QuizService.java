package com.klyagin.service;


import com.klyagin.model.Quiz;

import java.util.List;

public interface QuizService {
    List<Quiz> findAllQuizes();
}
