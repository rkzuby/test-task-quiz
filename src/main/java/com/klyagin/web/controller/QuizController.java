package com.klyagin.web.controller;

import com.klyagin.model.Quiz;
import com.klyagin.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class QuizController {
    @Autowired
    private QuizService quizService;

    @RequestMapping("/")
    public String redirect(){
        return "redirect:/quiz";
    }

    @RequestMapping("/quiz")
    public String quiz(Model model){
        List<Quiz> quizList = quizService.findAllQuizes();
        model.addAttribute("quizes", quizList);
        return "quiz";
    }
}
