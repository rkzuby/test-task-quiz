package com.klyagin.dao;

import com.klyagin.model.Quiz;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizDao extends CrudRepository<Quiz, Long> {
    List<Quiz> findAll();
}
