-- Insert quiz
insert into quiz (quizName) values ('Test "На лучшем языке писать имеешь право"');

-- Insert questions
insert into question (questionText, quiz_id) values ('Что такое НТР?', 1);
insert into question (questionText, quiz_id) values ('Бывает ли так, что программа скомпилировалась с первого раза и без ошибок?', 1);
insert into question (questionText, quiz_id) values ('Какой правильный ответ на вопрос про рекурсию?', 1);
insert into question (questionText, quiz_id) values ('Вы пришли на проект, над которым раньше работал другой программист. Что можно сказать о его коде?', 1);
insert into question (questionText, quiz_id) values ('Что такое Пик Балмера?', 1);
insert into question (questionText, quiz_id) values ('Что такое стринги?', 1);
insert into question (questionText, quiz_id) values ('Какой вопрос?', 1);
insert into question (questionText, quiz_id) values ('Это последний вопрос?', 1);
insert into question (questionText, quiz_id) values ('Не совсем... Что такое баг?', 1);

-- Insert answers
insert into answer (answerText, correct, question_id) values ('Научно-техническая революция', false, 1);
insert into answer (answerText, correct, question_id) values ('Научно-техническая разработка', false, 1);
insert into answer (answerText, correct, question_id) values ('Научно-техническая рэп', true, 1);
insert into answer (answerText, correct, question_id) values ('Научно-технический рок', false, 1);

insert into answer (answerText, correct, question_id) values ('Да', false, 2);
insert into answer (answerText, correct, question_id) values ('У меня бывало', false, 2);
insert into answer (answerText, correct, question_id) values ('Нет', true, 2);

insert into answer (answerText, correct, question_id) values ('Да', false, 3);
insert into answer (answerText, correct, question_id) values ('33', false, 3);
insert into answer (answerText, correct, question_id) values ('Какой правильный ответ на вопрос про рекурсию?', true, 3);

insert into answer (answerText, correct, question_id) values ('Надо сначала детально изучить проект, чтобы понять это', false, 4);
insert into answer (answerText, correct, question_id) values ('НУ КТО ТАК ПИШЕТ?!?!?!', true, 4);

insert into answer (answerText, correct, question_id) values ('Гора в Северной Америке', false, 5);
insert into answer (answerText, correct, question_id) values ('Феномен о том, что при определённой концентрации алкоголя в крови программистские способности резко возрастают', true, 5);
insert into answer (answerText, correct, question_id) values ('Яхта Стива Балмера — бывшего генерального директора Microsoft', false, 5);

insert into answer (answerText, correct, question_id) values ('Трусы', false, 6);
insert into answer (answerText, correct, question_id) values ('Веревки', false, 6);
insert into answer (answerText, correct, question_id) values ('Переменные типа «строка»', true, 6);

insert into answer (answerText, correct, question_id) values ('Такой и ответ', true, 7);

insert into answer (answerText, correct, question_id) values ('Да', true, 8);
insert into answer (answerText, correct, question_id) values ('Да', true, 8);
insert into answer (answerText, correct, question_id) values ('Да', true, 8);
insert into answer (answerText, correct, question_id) values ('Да', true, 8);

insert into answer (answerText, correct, question_id) values ('Ошибка', false, 9);
insert into answer (answerText, correct, question_id) values ('Жук', false, 9);
insert into answer (answerText, correct, question_id) values ('Фича', false, 9);
insert into answer (answerText, correct, question_id) values ('Вот это', true, 9);